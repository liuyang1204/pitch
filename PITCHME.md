# Yosolo Report
## Nov. 2

---

## Status

- 7 environments
- 99 applications
- 118 deployments
- 115 registered users
- 207 GiB used memory

---

## Progress

- Migrated to new OpenStack completely
- Upgraded to Kubernetes 1.8 with high availability setup

+++

## Portal
- `Environments` page
- Show and follow logs
- Update/delete/restart actions

---

## Stability

- Yosolo API service and Portal became very stable
- Kubernetes master nodes are now HA

+++

## Problems

- Pod status `Unknown`, `Pending`, `Terminating`
- Gitlab sometimes goes down

+++

## Reason

**Kubernetes node is not stable because of low performance instances of OpenStack.**

The node is sharing the resources with other instances and sometimes becomes `NotReady` by the effect of them. 

+++

## Actions

- Improve OpenStack instance setup (bare metal machine)
- Create problem detection and auto-healing system

---

## Planned features

- Feature branch deployment
- Monitoring summary on Portal

---

## Suggestions

- Set memory & CPU requests and limits
- Do not download war files in Docker images, instead, put the war file into the image

---

## Technologies

- Kubernetes
- Golang
- Vue JS
- TypeScript

---

# Thank you!
